package elements;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class JsValidation {
    public SelenideElement
            nameValidation = $("#name-error"),
            phoneValidation = $("#phone-error"),
            emailValidation = $("#email-error"),
            cityValidation = $("#city-error"),
            streetValidation = $("#street-error"),
            telValidation = $("#tel-error");
}
