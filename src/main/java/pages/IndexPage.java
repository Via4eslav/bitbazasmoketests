package pages;

import com.codeborne.selenide.SelenideElement;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class IndexPage {
    public SelenideElement
            callBackModal = $(".button.button--sm._mr-lg.js-mfp-ajax"),
            nameInput = $(".form__input.control-holder>input"),
            phoneInput = $(".js-inputmask"),
            sendFormButton = $(".button.button--send.js-form-submit"),
            consultationModal = $(".screen__cnt .button"),
            emailInput = $x("html/body/div[2]/div/div[1]/div/div/div[3]/div[3]/div/input"),
            nameQuestionForm = $x("/html/body/div[2]/main/section[10]/div[2]/div/div/div[3]/" +
                    "div[1]/div/input"),
            phoneQuestionForm = $x("/html/body/div[2]/main/section[10]/div[2]/div/div/div[3]/" +
                    "div[2]/div/input"),
            emailQuestionForm = $x("/html/body/div[2]/main/section[10]/div[2]/div/div/div[3]/div[3]/" +
                    "div/input"),
            submitQuestionForm = $x("/html/body/div[2]/main/section[10]/div[2]/div/div/div[3]/button");

    public void clickCallBackModal() {
        callBackModal.click();
    }

    public void fillNameInput(String userName) {
        nameInput.val(userName);
    }

    public void fillPhoneInput(String userPhone) {
        phoneInput.val(userPhone);
    }

    public void clickSendForm() {
        sendFormButton.click();
    }

    public void clickConsultationModal() {
        consultationModal.click();
    }

    public void fillEmailInput(String userEmail) {
        Random r = new Random();
        char a = (char) (r.nextInt(26) + 'a');
        char b = (char) (r.nextInt(26) + 'b');
        emailInput.val(a + b + userEmail + b + a + b + ".com");
    }

    //    "Any Question" Form?
    public void fillNameQuestionForm(String userName) {
        nameQuestionForm.val(userName);
    }

    public void fillPhoneQuestionForm(String userPhone) {
        phoneQuestionForm.val(userPhone);
    }

    public void fillEmailQuestionForm(String userEmail) {
        Random r = new Random();
        char a = (char) (r.nextInt(26) + 'a');
        emailQuestionForm.val(a + userEmail + a + ".com");
    }

    public void fillSubmitQuestionForm() {
        submitQuestionForm.click();
    }
}
