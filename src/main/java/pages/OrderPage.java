package pages;

import base.TestBase;
import com.codeborne.selenide.SelenideElement;

import java.util.Random;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class OrderPage extends TestBase {
    public SelenideElement
            addToCart = $x("html/body/div[1]/div[2]/div/section[2]/div[2]/div/div[2]/div[4]/button"),
            cartModal = $x("/html/body/div[1]/div[1]/header/div[2]/div/div/div[2]/div/div[1]/div[1]"),
            makeOrder = $x("html/body/div[1]/div[1]/header/div[2]/div/div/div[2]/div/div[2]/div[2]/div/a"),
            goToSecondStep = $x("html/body/div[1]/div[2]/div/section[2]/div/div/div[1]/div/div[2]/a"),
            nameInput = $(byName("name")),
            emailInput = $(byName("email")),
            phoneInput = $(byName("tel")),
            cityInput = $(byName("city")),
            streetInput = $(byName("street")),
            orderButton = $x("html/body/div[1]/div[2]/div/section[2]/div/div/div/div[2]/div/div[2]/div/div[3]/div/div[2]/button");

    public void clickAddToCart() {
        addToCart.click();
    }

    public void clickCartModal() {
        cartModal.click();
    }

    public void clickMakeOrder() {
        makeOrder.click();
    }

    public void clickGoToSecondStep() {
        goToSecondStep.click();
    }

    public void fillNameInput(String userName) {
        nameInput.val(userName);
    }

    public void fillEmailInput(String userEmail) {
        Random r = new Random();
        char a = (char) (r.nextInt(26) + 'a');
        emailInput.val(a + userEmail + a + ".com");
    }

    public void fillPhoneInput(String userPhone) {
        phoneInput.val(userPhone);
    }

    public void fillCityInput(String userCity) {
        cityInput.val(userCity);
    }

    public void fillStreetInput(String userStreet) {
        streetInput.val(userStreet);
    }

    public void clickOrderButton() {
        orderButton.click();
    }
}
