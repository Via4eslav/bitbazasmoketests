package pages;

import com.codeborne.selenide.SelenideElement;

import java.util.Random;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class ContactsPage {
    public SelenideElement
            nameInput = $(".form__input.control-holder>input"),
            phoneInput = $(".js-inputmask"),
            emailInput = $(byName("email")),
            sendFormButton = $(".button.button--send.js-form-submit");

    public void fillNameInput(String userName) {

        nameInput.val(userName);
    }

    public void fillPhoneInput(String userPhone) {
        phoneInput.val(userPhone);
    }

    public void fillEmailInput(String userEmail) {
        $(emailInput).scrollTo();
        Random r = new Random();
        char a = (char) (r.nextInt(26) + 'a');
        char b = (char) (r.nextInt(26) + 'b');
        emailInput.val(a + b + userEmail + b + a + b + ".com");
    }

    public void clickSendForm() {
        sendFormButton.click();
    }
}
