package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class ItemPage {
    public SelenideElement
            nameManagerForm = $x("/html/body/div[1]/div[2]/div/section[2]/div[2]/div/div[3]/div/" +
            "div[2]/div[5]/div/div[1]/div/input"),
            phoneManagerForm = $x("/html/body/div[1]/div[2]/div/section[2]/div[2]/div/div[3]" +
                    "/div/div[2]/div[5]/div/div[2]/div/input"),
            submitManagerForm = $x("/html/body/div[1]/div[2]/div/section[2]/div[2]/" +
                    "div/div[3]/div/div[2]/div[5]/div/button");

    public void fillNameManagerForm(String userName) {
        nameManagerForm.val(userName);
    }

    public void fillPhoneManagerForm(String userPhone) {
        phoneManagerForm.val(userPhone);
    }

    public void clickSubmitManagerForm() {
        submitManagerForm.click();
    }
}
