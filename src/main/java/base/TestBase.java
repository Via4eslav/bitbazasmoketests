package base;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Configuration.startMaximized;
import static com.codeborne.selenide.Selenide.close;


/**
 * Created by ivanov.v on 22.01.2018.
 */
public class TestBase {
    public String baseUrl = "http://1:1@bitbaza.wezom.net";

    @BeforeClass
    public void start() {
        browser = "chrome";
        startMaximized = true;
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        Configuration.timeout = 6000;
        clearBrowserCookies();
    }

    @AfterMethod
    public void tearDown() {
        close();
    }
}
