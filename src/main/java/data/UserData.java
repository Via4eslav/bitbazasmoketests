package data;

/**
 * Created by ivanov.v on 26.01.2018.
 */
public class UserData {

                /* Valid data */

    public String userName = "Test User";
    public String userEmail = "user@test";
    public String userPhone = "8005553535";
    public String userCity = "Test City";
    public String userStreet = "Test Street";

                /* invalid data */

    public String invalidUserName = "11";
    public String oneSymbolUserName = "u";
    public String invalidUserEmail = "user*test";
    public String invalidUserPhone = "800";
    public String invalidUserCity = "12";
    public String invalidUserStreet = "T";
}
