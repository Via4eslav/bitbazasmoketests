package testcases;

import base.TestBase;
import data.Links;
import data.UserData;
import elements.JsValidation;
import elements.Notifications;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ItemPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class MangerFormAtItemPageTest extends TestBase {
    public ItemPage itemPage;
    public UserData userData;
    public Links links;
    public Notifications notifications;
    public JsValidation jsValidation;

    @BeforeMethod
    public void setUp() {
        itemPage = new ItemPage();
        userData = new UserData();
        links = new Links();
        notifications = new Notifications();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 33)
    public void userSendsConsultingFormTest() {
        open(baseUrl + links.itemLink );
        itemPage.fillNameManagerForm(userData.userName);
        itemPage.fillPhoneManagerForm(userData.userPhone);
        itemPage.clickSubmitManagerForm();
        notifications.successMessage.shouldBe(visible).getText();
        System.out.println(notifications.successMessage);
//        notifications.successMessage.shouldBe(visible).shouldHave(text("Администрация сайта скоро Вам перезвонит!"));
    }

    /* One symbol in name input */

    @Test(priority = 34)
    public void managerFormWithOneSymbolInNameInputTest() {
        open(baseUrl + links.itemLink );
        itemPage.fillNameManagerForm(userData.oneSymbolUserName);
        itemPage.fillPhoneManagerForm(userData.userPhone);
        itemPage.clickSubmitManagerForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid name */

    @Test(priority = 35)
    public void managerFormInvalidNameTest() {
        open(baseUrl + links.itemLink );
        itemPage.fillNameManagerForm(userData.invalidUserName);
        itemPage.fillPhoneManagerForm(userData.userPhone);
        itemPage.clickSubmitManagerForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* Invalid phone */

    @Test(priority = 36)
    public void managerFormInvalidPhoneTest() {
        open(baseUrl + links.itemLink );
        itemPage.fillNameManagerForm(userData.userName);
        itemPage.fillPhoneManagerForm(userData.invalidUserPhone);
        itemPage.clickSubmitManagerForm();
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Empty inputs */

    @Test(priority = 37)
    public void managerFormEmptyInputsTest() {
        open(baseUrl + links.itemLink );
        itemPage.clickSubmitManagerForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
