package testcases;

import base.TestBase;
import data.Links;
import data.UserData;
import elements.JsValidation;
import elements.Notifications;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ContactsPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class ContactsTest extends TestBase {
    public ContactsPage contactsPage;
    public UserData userData;
    public Links links;
    public Notifications notifications;
    public JsValidation jsValidation;

    @BeforeMethod
    public void setUp() {
        contactsPage = new ContactsPage();
        userData = new UserData();
        links = new Links();
        notifications = new Notifications();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 13)
    public void userSendsContactFormTest() {
        open(baseUrl + links.contactLink);
        contactsPage.fillNameInput(userData.userName);
        contactsPage.fillPhoneInput(userData.userPhone);
        contactsPage.fillEmailInput(userData.userEmail);
        contactsPage.clickSendForm();
        notifications.successMessage.shouldBe(visible).getText();
        System.out.println(notifications.successMessage);
//        notifications.successMessage.shouldBe(visible).shouldHave(text("Администрация сайта скоро Вам перезвонит!"));
    }

    /* Invalid name */

    @Test(priority = 14)
    public void contactsInvalidNameTest() {
        open(baseUrl + links.contactLink);
        contactsPage.fillNameInput(userData.invalidUserName);
        contactsPage.fillPhoneInput(userData.userPhone);
        contactsPage.fillEmailInput(userData.userPhone);
        contactsPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* One symbol in name input */

    @Test(priority = 15)
    public void contactsWithOneSymbolInNameInputTest() {
        open(baseUrl + links.contactLink);
        contactsPage.fillNameInput(userData.oneSymbolUserName);
        contactsPage.fillPhoneInput(userData.userPhone);
        contactsPage.fillEmailInput(userData.userEmail);
        contactsPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid phone */

    @Test(priority = 16)
    public void contactsInvalidPhoneTest() {
        open(baseUrl + links.contactLink);
        contactsPage.fillNameInput(userData.userName);
        contactsPage.fillPhoneInput(userData.invalidUserPhone);
        contactsPage.fillEmailInput(userData.userEmail);
        contactsPage.clickSendForm();
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Invalid email */

    @Test(priority = 17)
    public void contactsInvalidEmailTest() {
        open(baseUrl + links.contactLink);
        contactsPage.fillNameInput(userData.userName);
        contactsPage.fillPhoneInput(userData.userPhone);
        contactsPage.fillEmailInput(userData.invalidUserEmail);
        contactsPage.clickSendForm();
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите корректный адрес" +
                " электронной почты!"));
    }

    /* Empty inputs */

    @Test(priority = 18)
    public void contactsEmptyInputsTest() {
        open(baseUrl + links.contactLink);
        contactsPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
