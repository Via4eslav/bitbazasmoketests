package testcases;

import base.TestBase;
import data.UserData;
import elements.JsValidation;
import elements.Notifications;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IndexPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class ConsultationFromIndexTest extends TestBase {
    public IndexPage indexPage;
    public UserData userData;
    public Notifications notifications;
    public JsValidation jsValidation;

    @BeforeMethod
    public void setUp() {
        indexPage = new IndexPage();
        userData = new UserData();
        notifications = new Notifications();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 7)
    public void userSendsConsultationFormTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.fillNameInput(userData.userName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.fillEmailInput(userData.userEmail);
        indexPage.clickSendForm();
        notifications.successMessage.shouldBe(visible).getText();
        System.out.println(notifications.successMessage);
//        notifications.successMessage.shouldBe(visible).shouldHave(text("Администрация сайта скоро Вам перезвонит!"));
    }

    /* Invalid name */

    @Test(priority = 8)
    public void consultInvalidNameTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.fillNameInput(userData.invalidUserName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.fillEmailInput(userData.userEmail);
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* One symbol in name input */

    @Test(priority = 9)
    public void consultOneSymbolInNameInputTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.fillNameInput(userData.oneSymbolUserName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.fillEmailInput(userData.userEmail);
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid phone */

    @Test(priority = 10)
    public void consultInvalidPhoneTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.fillNameInput(userData.userName);
        indexPage.fillPhoneInput(userData.invalidUserPhone);
        indexPage.fillEmailInput(userData.userEmail);
        indexPage.clickSendForm();
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Invalid email */

    @Test(priority = 11)
    public void consultInvalidEmailTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.fillNameInput(userData.userName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.fillEmailInput(userData.invalidUserEmail);
        indexPage.clickSendForm();
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите корректный адрес" +
                " электронной почты!"));
    }

    /* Empty inputs */

    @Test(priority = 12)
    public void consultEmptyInputsTest() {
        open(baseUrl);
        indexPage.clickConsultationModal();
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
