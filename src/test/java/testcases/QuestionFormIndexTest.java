package testcases;

import base.TestBase;
import data.UserData;
import elements.JsValidation;
import elements.Notifications;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IndexPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class QuestionFormIndexTest extends TestBase {
    public IndexPage indexPage;
    public UserData userData;
    public Notifications notifications;
    public JsValidation jsValidation;

    @BeforeMethod
    public void setUp() {
        indexPage = new IndexPage();
        userData = new UserData();
        notifications = new Notifications();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 27)
    public void userSendsQuestionFormTest() {
        open(baseUrl);
        indexPage.fillNameQuestionForm(userData.userName);
        indexPage.fillPhoneQuestionForm(userData.userPhone);
        indexPage.fillEmailQuestionForm(userData.userEmail);
        indexPage.fillSubmitQuestionForm();
        notifications.successMessage.shouldBe(visible).getText();
        System.out.println(notifications.successMessage);
//        notifications.successMessage.shouldBe(visible).shouldHave(text("Администрация сайта скоро Вам перезвонит!"));
    }

    /* One symbol in name input */

    @Test(priority = 28)
    public void questionFormWithOneSymbolInNameInputTest() {
        open(baseUrl);
        indexPage.fillNameQuestionForm(userData.oneSymbolUserName);
        indexPage.fillPhoneQuestionForm(userData.userPhone);
        indexPage.fillEmailQuestionForm(userData.userEmail);
        indexPage.fillSubmitQuestionForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid name */

    @Test(priority = 29)
    public void questionFormInvalidName() {
        open(baseUrl);
        indexPage.fillNameQuestionForm(userData.invalidUserName);
        indexPage.fillPhoneQuestionForm(userData.userPhone);
        indexPage.fillEmailQuestionForm(userData.userEmail);
        indexPage.fillSubmitQuestionForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* Invalid phone */

    @Test(priority = 30)
    public void questionFormInvalidPhoneTest() {
        open(baseUrl);
        indexPage.fillNameQuestionForm(userData.userName);
        indexPage.fillPhoneQuestionForm(userData.invalidUserPhone);
        indexPage.fillEmailQuestionForm(userData.userEmail);
        indexPage.fillSubmitQuestionForm();
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Invalid email */

    @Test(priority = 31)
    public void questionFormInvalidEmailTest() {
        open(baseUrl);
        indexPage.fillNameQuestionForm(userData.userName);
        indexPage.fillPhoneQuestionForm(userData.userPhone);
        indexPage.fillEmailQuestionForm(userData.invalidUserEmail);
        indexPage.fillSubmitQuestionForm();
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите корректный адрес " +
                "электронной почты!"));
    }

    /* Empty inputs */

    @Test(priority = 32)
    public void questionFormEmptyInputsTest() {
        open(baseUrl);
        indexPage.fillSubmitQuestionForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
