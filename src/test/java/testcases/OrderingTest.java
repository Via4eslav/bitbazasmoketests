package testcases;

import base.TestBase;
import data.Links;
import data.UserData;
import elements.JsValidation;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.OrderConfirmPage;
import pages.OrderPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class OrderingTest extends TestBase {
    public OrderPage orderPage;
    public UserData userData;
    public Links links;
    public OrderConfirmPage orderConfirmPage;
    public JsValidation jsValidation;

    @BeforeMethod
    public void setUp() {
        orderPage = new OrderPage();
        userData = new UserData();
        links = new Links();
        orderConfirmPage = new OrderConfirmPage();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 19)
    public void userMakesOrderTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.userName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        orderConfirmPage.successTitle.shouldHave(text("Менеджер свяжется с вами для уточнения деталей с 9:00‒18:00 " +
                "по московскому времени. Спасибо за ваш заказ!"));
    }

    /* One symbol in name input */

    @Test(priority = 20)
    public void orderWithOneSymbolInNameInputTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.oneSymbolUserName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid name */

    @Test(priority = 21)
    public void orderWithInvalidNameTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.invalidUserName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* Invalid email */

    @Test(priority = 22)
    public void orderWithInvalidEmailTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.userName);
        orderPage.fillEmailInput(userData.invalidUserEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите корректный адрес " +
                "электронной почты!"));
    }

    /* Invalid phone */

    @Test(priority = 23)
    public void orderWithInvalidPhoneTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.userName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.invalidUserPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        jsValidation.telValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Invalid city */

    @Test(priority = 24)
    public void orderWithInvalidCityTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.userName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.invalidUserCity);
        orderPage.fillStreetInput(userData.userStreet);
        orderPage.clickOrderButton();
        jsValidation.cityValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* Invalid street */

    @Test(priority = 25)
    public void orderWithInvalidStreetTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.fillNameInput(userData.userName);
        orderPage.fillEmailInput(userData.userEmail);
        orderPage.fillPhoneInput(userData.userPhone);
        orderPage.fillCityInput(userData.userCity);
        orderPage.fillStreetInput(userData.invalidUserStreet);
        orderPage.clickOrderButton();
        jsValidation.streetValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Empty inputs */

    @Test(priority = 26)
    public void orderWithEmptyInputsTest() {
        open(baseUrl + links.itemLink );
        orderPage.clickAddToCart();
        orderPage.clickCartModal();
        orderPage.clickMakeOrder();
        orderPage.clickGoToSecondStep();
        orderPage.clickOrderButton();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.emailValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.telValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.cityValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.streetValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
