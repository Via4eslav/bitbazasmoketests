package testcases;

import base.TestBase;
import data.UserData;
import elements.JsValidation;
import elements.Notifications;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IndexPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 22.01.2018.
 */
public class CallBackTest extends TestBase {
    public IndexPage indexPage;
    public Notifications notifications;
    public JsValidation jsValidation;
    public UserData userData;

    @BeforeMethod
    public void setUp() {
        indexPage = new IndexPage();
        userData = new UserData();
        notifications = new Notifications();
        jsValidation = new JsValidation();
    }

    /* Valid data */

    @Test(priority = 1)
    public void userSendsCallBackFormTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.fillNameInput(userData.userName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.clickSendForm();
        notifications.successMessage.shouldBe(visible).getText();
        System.out.println(notifications.successMessage);
//        notifications.successMessage.shouldBe(visible).shouldHave(text("Администрация сайта скоро Вам перезвонит!"));
    }

    /* Invalid name */

    @Test(priority = 2)
    public void callBackInvalidNameTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.fillNameInput(userData.invalidUserName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
    }

    /* One symbol in name input */

    @Test(priority = 3)
    public void callBackOneSymbolInNameInputTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.fillNameInput(userData.oneSymbolUserName);
        indexPage.fillPhoneInput(userData.userPhone);
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Пожалуйста, введите не менее 2 символов!"));
    }

    /* Invalid phone */

    @Test(priority = 4)
    public void callBackInvalidPhoneTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.fillNameInput(userData.userName);
        indexPage.fillPhoneInput(userData.invalidUserPhone);
        indexPage.clickSendForm();
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Invalid data in both inputs */

    @Test(priority = 5)
    public void callBackInvalidDataInBothFieldsTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.fillNameInput(userData.invalidUserName);
        indexPage.fillPhoneInput(userData.invalidUserPhone);
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Введите корректное словесное значение!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Введите коректный номер телефона"));
    }

    /* Empty inputs */

    @Test(priority = 6)
    public void callBackEmptyInputsTest() {
        open(baseUrl);
        indexPage.clickCallBackModal();
        indexPage.clickSendForm();
        jsValidation.nameValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
        jsValidation.phoneValidation.shouldBe(visible).shouldHave(text("Это поле необходимо заполнить!"));
    }
}
